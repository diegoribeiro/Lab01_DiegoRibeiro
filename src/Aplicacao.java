import edu.cest.lab01.Pessoa;
import edu.cest.lab01.Cliente;
import edu.cest.lab01.Animal;
public class Aplicacao {
	public static void main(String[] args){
	Pessoa pessoa1 = new Pessoa();
	pessoa1.nome = "Carlos";
	Cliente cliente1 = new Cliente();
	cliente1.nome = "Diego";
	Animal animal1 = new Animal();
	animal1.nome = "Cerberos";
	System.out.println("Nome da Pessoa: " + pessoa1.nome);
	System.out.println("Nome do Cliente: " + cliente1.nome);
	System.out.println("Nome do Animal: " + animal1.nome);}
}
